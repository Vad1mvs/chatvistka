package com.lesson.vadim.chatgray.chats;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.lesson.vadim.chatgray.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lesson.vadim.chatgray.custompackage.CustomActionBar;
import com.lesson.vadim.chatgray.utils.Const;


public class OverallChat extends CustomActionBar {
    private ArrayList<Conversation> convList;

    private ChatAdapter adp;
    private EditText text;
    private String buddy;
    private Date lastMsgDate;
    private Button btnSend;
    private boolean isRunning;
    private static Handler handler;
    Typeface font2;
    TextView message, name, check, time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overall_chat);

        convList = new ArrayList<Conversation>();
        ListView list = (ListView) findViewById(R.id.list);
        font2 = Typeface.createFromAsset(getAssets(), "Jakob2.ttf");

        adp = new ChatAdapter();
        list.setAdapter(adp);
        list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        list.setStackFromBottom(true);

        text = (EditText) findViewById(R.id.txt);
        text.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        text.setTypeface(font2);

        btnSend = (Button)findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnSend) {
                    sendMessage();
                }
            }
        });

        setTouchNClick(R.id.btnSend);

        buddy = getIntent().getStringExtra(Const.EXTRA_DATA);
        getActionBar().setTitle(buddy);

        handler = new Handler();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
       // isRunning = true;
        //loadConversationList();
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        isRunning = false;
    }

    @Override
    public void onClick(View v)
    {
        super.onClick(v);
        if (v.getId() == R.id.btnSend)
        {
            sendMessage();
            loadConversationList();
        }

    }


    private void sendMessage()
    {
        if (text.length() == 0)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(text.getWindowToken(), 0);

        String s = text.getText().toString();
        final Conversation c = new Conversation(s, new Date(), UserList.user.getUsername());
        c.setStatus(Conversation.STATUS_SENDING);
        convList.add(c);
        adp.notifyDataSetChanged();
        text.setText(null);

        ParseObject po = new ParseObject("OverallChat");
        po.put("sender", UserList.user.getUsername());
        po.put("message", s);
        po.saveEventually(new SaveCallback() {

            @Override
            public void done(ParseException e)
            {
                if (e == null)
                    c.setStatus(Conversation.STATUS_SENT);
                else
                    c.setStatus(Conversation.STATUS_FAILED);
                adp.notifyDataSetChanged();
            }
        });
    }


    private void loadConversationList()
    {
        ParseQuery<ParseObject> q = ParseQuery.getQuery("OverallChat");
        if (convList.size() == 0)
        {
            ArrayList<String> al = new ArrayList<>();
            al.add(buddy);
            al.add(UserList.user.getUsername());
            q.whereContainedIn("sender", al);
            q.whereContainedIn("message", al);
        }
        else
        {
        }
        q.orderByDescending("createdAt");
        q.setLimit(30);
        q.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> li, ParseException e) {
                if (li != null && li.size() > 0) {
                    for (int i = li.size() - 1; i >= 0; i--) {
                        ParseObject po = li.get(i);
                        Conversation c = new Conversation(po.getString("message"), po.getCreatedAt(), po.getString("sender"));
                        convList.add(c);
                        if (lastMsgDate == null || lastMsgDate.before(c.getDate())) lastMsgDate = c.getDate();
                        adp.notifyDataSetChanged();
                    }
                }
            }
        });

    }




    private class ChatAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return convList.size();
        }

        @Override
        public Object getItem(int arg0)
        {
            return convList.get(arg0);
        }

        @Override
        public long getItemId(int arg0)
        {
            return arg0;
        }

        @Override
        public View getView(int pos, View v, ViewGroup arg2)
        {
            Conversation c = (Conversation) getItem(pos);
            if (c.isSent())
                v = getLayoutInflater().inflate(R.layout.chat_item_sent, null);
            else
                v = getLayoutInflater().inflate(R.layout.chat_item_rcv, null);

            time = (TextView) v.findViewById(R.id.time);
            time.setText(DateUtils.getRelativeDateTimeString(OverallChat.this, c.getDate().getTime(), 0, DateUtils.FORMAT_NUMERIC_DATE, 0));
            message = (TextView) v.findViewById(R.id.message);
            message.setText(c.getMsg());
            name = (TextView)v.findViewById(R.id.name);
            name.setText(c.getSender());
            name.setTypeface(font2);
            message.setTypeface(font2);
            check = (TextView) v.findViewById(R.id.check);

            if (c.isSent())
            {
                if (c.getStatus() == Conversation.STATUS_SENT)
                    check.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.checkdone, 0, 0);
                else if (c.getStatus() == Conversation.STATUS_SENDING)
                    check.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.checksend, 0, 0);
                else
                    check.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.nosend, 0, 0);
            }
            else
                check.setText("");

            return v;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
