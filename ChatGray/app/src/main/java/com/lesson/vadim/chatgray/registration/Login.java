package com.lesson.vadim.chatgray.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lesson.vadim.chatgray.R;
import com.lesson.vadim.chatgray.chats.UserList;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import com.lesson.vadim.chatgray.custompackage.CustomActionBar;


public class Login extends CustomActionBar {
    private EditText user, pwd;
    private Button btnReg, btnLogin;
    Typeface font;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getActionBar().setDisplayHomeAsUpEnabled(false);

        font = Typeface.createFromAsset(getAssets(), "Jakob2.ttf");

        user = (EditText)findViewById(R.id.user);
        pwd = (EditText)findViewById(R.id.pwd);
        btnReg = (Button)findViewById(R.id.btnReg);
        btnLogin = (Button)findViewById(R.id.btnLogin);

        btnLogin.setTypeface(font);
        btnReg.setTypeface(font);
        user.setTypeface(font);
        pwd.setTypeface(font);


        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,Register.class);
                startActivityForResult(intent, 5);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = user.getText().toString().trim();
                String password = pwd.getText().toString().trim();

                if(username.length() == 0 || password.length() == 0){
                    UtilsDialog.showDialog(Login.this, R.string.err_fields_empty);
                    return;
                }
                final ProgressDialog dia = ProgressDialog.show(Login.this, null, getString(R.string.alert_wait));

                ParseUser.logInInBackground(username, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser pu, ParseException e) {

                            dia.dismiss();
                            if (pu != null) {
                                UserList.user = pu;
                                Intent intent = new Intent(Login.this, UserList.class);
                                startActivity(intent);
                                finish();
                            } else {
                                UtilsDialog.showDialog(Login.this, getString(R.string.err_login) + " " + e.getMessage());
                                e.printStackTrace();
                            }

                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10 && resultCode == RESULT_OK)
            finish();
    }
}


