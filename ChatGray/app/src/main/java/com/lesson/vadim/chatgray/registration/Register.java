package com.lesson.vadim.chatgray.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lesson.vadim.chatgray.R;
import com.lesson.vadim.chatgray.chats.UserList;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import com.lesson.vadim.chatgray.custompackage.CustomActionBar;


public class Register extends CustomActionBar {
    private EditText user, pwd, pwd2, email;
    Button btnReg;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        getActionBar().setDisplayHomeAsUpEnabled(false);
        font = Typeface.createFromAsset(getAssets(), "Jakob2.ttf");
        user = (EditText)findViewById(R.id.user);
        pwd = (EditText)findViewById(R.id.pwd);
        pwd2 = (EditText)findViewById(R.id.pwd2);
        email = (EditText)findViewById(R.id.email);

        btnReg = (Button)findViewById(R.id.btnReg);
        btnReg.setTypeface(font);
        user.setTypeface(font);
        pwd.setTypeface(font);
        pwd2.setTypeface(font);
        email.setTypeface(font);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u = user.getText().toString();
                String p = pwd.getText().toString();
                String p2 = pwd2.getText().toString();
                String e = email.getText().toString();

                if(u.length() == 0 || p.length() == 0 || e.length() == 0){
                    UtilsDialog.showDialog(Register.this, R.string.err_fields_empty);
                    return;
                }
                boolean t;
                if(p.equals(p2)){
                    t = true;
                }else {
                    t = false;
                    UtilsDialog.showDialog(Register.this, "Pleas enter the same password twice");
                    return;
                }

                final ProgressDialog dia = ProgressDialog.show(Register.this, null, getString(R.string.alert_wait));
                final ParseUser pu = new ParseUser();
                pu.setEmail(e);
                pu.setPassword(p);
                pu.setUsername(u);
                pu.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        dia.dismiss();
                        if(e == null){
                            UserList.user = pu;
                            startActivity(new Intent(Register.this, UserList.class));
                            setResult(RESULT_OK);
                            finish();
                        }else{
                            UtilsDialog.showDialog(Register.this, getString(R.string.err_singup) + " " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

}
